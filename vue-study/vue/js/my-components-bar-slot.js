Vue.component('header-bar', {
    data: function() {
        return {
            title: "三国 -- √"
        }
    },
    template: `<div class="divStyle">
                <table class="tableStyle">
                    <tr>
                        <td width="200" align="right" valign="middle">
                            <img src="img/1.jpg" class="logoImg" />
                        </td>
                        <td>
                            <label class="nameStyle">
                                登录人： {{name}}
                            </label>
                        </td>
                        <td>
                            <slot></slot>
                        </td>
                        <td>
                            <button @click="childMethod">组件按钮点击次数</button>
                        </td>
                    </tr>
                </table>
            </div>`,
    props: ["name"],
    methods: {
        childMethod: function () {
            this.$emit("my-event", this.title);
        }
    }
});


Vue.component('page-frame', {
    template:  `<div>
            <div id="header" style="width: 100%; height: 100px; background: pink">
                {{title}}<br />
                slot1 : <br />
                <slot name="slot1"></slot>
            </div>
            <div style="width: 100%; height: 580px">
                slot2 : <br />
                <slot name="slot2"></slot>
            </div>
            <div id="footer" style="width: 100%; height: 40px; background: lightblue">{{cr}}</div>
        </div>`,
    props: ["title", "cr"]
});

Vue.component('page-frame-scope', {
    template:  `<div>
            <div id="header" style="width: 100%; height: 100px; background: pink">
                {{title}}<br />
                slot1 : <br />
                <slot name="slot1"></slot>
            </div>
            <div style="width: 100%; height: 580px">
                slot2 : <br />
                <slot name="slot2" v-bind:sites="sites"></slot>
            </div>
            <div id="footer" style="width: 100%; height: 40px; background: lightblue">{{cr}}</div>
        </div>`,
    props: ["title", "cr"],
    data: function () {
        return {
            sites: [
                {
                    "name": "菜鸟教程",
                    "url": "www.runoob.com"
                },
                {
                    "name": "google",
                    "url": "www.google.com"
                },
                {
                    "name": "微博",
                    "url": "www.weibo.com"
                }
            ]
        }
    }
});