Vue.component('header-bar', {
    template: `<div class="divStyle">
                <table class="tableStyle">
                    <tr>
                        <td width="200" align="right" valign="middle">
                            <img src="img/1.jpg" class="logoImg" />
                        </td>
                        <td>
                            <label class="nameStyle">
                                登录人： {{name}}
                            </label>
                        </td>
                        <td>
                            <button @click="test">组件按钮点击次数</button>
                        </td>
                    </tr>
                </table>
            </div>`,
    props: ["name"],
    methods: {
        test: function () {
            alert("组件中 header-button 定义的函数事件")
        }
    }
});