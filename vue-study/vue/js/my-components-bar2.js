Vue.component('header-bar', {
    data: function() {
        return {
            title: "三国 -- √"
        }
    },
    template: `<div class="divStyle">
                <table class="tableStyle">
                    <tr>
                        <td width="200" align="right" valign="middle">
                            <img src="img/1.jpg" class="logoImg" />
                        </td>
                        <td>
                            <label class="nameStyle">
                                登录人： {{name}}
                            </label>
                        </td>
                        <td>
                            <button @click="childMethod">组件按钮点击次数</button>
                        </td>
                    </tr>
                </table>
            </div>`,
    props: ["name"],
    methods: {
        childMethod: function () {
            this.$emit("my-event", this.title);
        }
    }
});