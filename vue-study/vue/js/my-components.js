Vue.component('header-button', {
    data: function () {
        // 组件中 data 是通过函数返回的对象
        return {
            name: "貂蝉",
            count: 0
        };
    },
    template: `<div class="divStyle">
                <table class="tableStyle">
                    <tr>
                        <td width="200" align="right" valign="middle">
                            <img src="img/1.jpg" class="logoImg" />
                        </td>
                        <td>
                            <label class="nameStyle">
                                登录人： {{name}}
                            </label>
                        </td>
                        <td>
                            <button @click="test(), count++">组件按钮点击{{count}}次数</button>
                        </td>
                    </tr>
                </table>
            </div>`,
    methods: {
        test: function () {
            alert("组件中 header-button 定义的函数事件")
        }
    }
});