import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// 导入 element ui
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import zhCn from "element-plus/es/locale/lang/zh-cn";

import "./assets/css/global.css";
import "./assets/css/member-background.css";

import VConsole from "vconsole"; // 引入
new VConsole(); // 初始化

createApp(App)
  .use(store)
  .use(router)
  .use(ElementPlus, { locale: zhCn, size: "small" })
  .mount("#app");
