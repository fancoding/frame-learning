import { createStore } from "vuex";

export default createStore({
  state: {
    consumer: {},
  },
  mutations: {
    SET_CONSUMER(state, consumer) {
      state.consumer = consumer;
    },
  },
  actions: {
    SET_CONSUMER({ commit }, consumer) {
      this.state.consumer = consumer;
    },
  },
  getters: {
    getConsumer: (state) => state.consumer,
  },
});
