import { createRouter, createWebHistory } from "vue-router";
import Layout from "../layout/Layout.vue";

const routes = [
  {
    path: "/",
    name: "Layout",
    component: Layout,
    redirect: "/home",
    children: [
      {
        path: "home",
        name: "Home",
        component: () => import("@/views/Home"),
      },
      {
        path: "consumer",
        name: "Consumer",
        component: () => import("@/views/Consumer"),
      },
      {
        path: "/book",
        name: "Book",
        component: () => import("@/views/Book"),
      },
      {
        path: "/news",
        name: "News",
        component: () => import("@/views/News"),
      },
    ],
  },
  // 登录 注册 找回密码
  {
    path: "/member",
    name: "Member",
    component: () => import("@/views/Member"),
  },

  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
