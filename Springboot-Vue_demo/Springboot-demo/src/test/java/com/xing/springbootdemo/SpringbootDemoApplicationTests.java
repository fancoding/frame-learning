package com.xing.springbootdemo;

import com.xing.springbootdemo.entity.Consumer;
import com.xing.springbootdemo.mapper.ConsumerMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@SpringBootTest
class SpringbootDemoApplicationTests {

    @Test
    void testLog(){
        log.info("#########  info  #########");
        log.debug("#########  debug  #########");
        log.error("#########  error  #########");
    }

    @Resource
    ConsumerMapper consumerMapper;
    @Test
    void contextLoads() {
        List<Consumer> consumers = consumerMapper.selectList(null);
        System.out.println(consumers);
    }

}
