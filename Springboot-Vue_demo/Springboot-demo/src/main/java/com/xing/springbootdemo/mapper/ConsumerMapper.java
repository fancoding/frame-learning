package com.xing.springbootdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xing.springbootdemo.entity.Consumer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: caidaxing
 * @Date: 2021/12/27/20:06
 * @Description:
 */
@Mapper
public interface ConsumerMapper extends BaseMapper<Consumer> {

    /**
     * 一对多查询
     * @param page
     * @param nickName
     * @return 用户分页列表查询，包含书籍表的一对多查询
     */
    Page<Consumer> findPage(Page<Consumer> page, @Param("nickName") String nickName);
}
