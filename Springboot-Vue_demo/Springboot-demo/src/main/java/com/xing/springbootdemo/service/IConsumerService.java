package com.xing.springbootdemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xing.springbootdemo.entity.Consumer;
import org.springframework.stereotype.Service;

/**
 * @Author: caidaxing
 * @Date: 2021/12/27/20:16
 * @Description:
 */
public interface IConsumerService extends IService<Consumer> {
}
