package com.xing.springbootdemo.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xing.springbootdemo.common.Result;
import com.xing.springbootdemo.entity.News;
import com.xing.springbootdemo.mapper.NewsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author: caidaxing
 * @Date: 2021/12/27/20:00
 * @Description:
 */
@RestController
@RequestMapping("/news")
public class NewsController {

    Logger logger = LoggerFactory.getLogger(NewsController.class);

    @Resource
    NewsMapper newsMapper;


    /**
     * 获取最新新闻信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<?> getById(@PathVariable Long id) {
        return Result.successWithData(newsMapper.selectById(id));
    }

    /**
     * 新增新闻
     * @param news
     * @return
     */
    @PostMapping
    public Result<?> save(@RequestBody News news) {
        try {
            news.setTime(new Date());
            newsMapper.insert(news);
            return Result.successWithoutData();
        }catch (Exception e) {
            logger.info("新增失败");
            return Result.failed("新增失败");
        }
    }

    /**
     * 更新新闻信息
     * @param news
     * @return
     */
    @PutMapping
    public Result<?> update(@RequestBody News news) {
        int i = newsMapper.updateById(news);
        if (i == 1) {
            return Result.successWithoutData();
        }
        logger.info("更新异常");
        return Result.failed("更新异常");
    }

    /**
     * 删除信息
     * @param id @PathVariable 注解来实现数据的获取
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Long id) {
        int i = newsMapper.deleteById(id);
        if (i == 1) {
            return Result.successWithoutData();
        }
        logger.info("删除异常");
        return Result.failed("删除异常");
    }

    /**
     * 查询 -> 按 名称 模糊查询
     * @param pageNum
     * @param pageSize
     * @param search
     * @return
     */
    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search) {
        // 模糊查询（昵称为 NULL ）也能匹配到数据
        LambdaQueryWrapper<News> wrapper = Wrappers.<News>lambdaQuery();
        if (StrUtil.isNotBlank(search)) {
            wrapper.like(News::getTitle, search);
        }
        Page<News> newsPage = newsMapper.selectPage(new Page<>(pageNum, pageSize), wrapper);
        return Result.successWithData(newsPage);
    }
}
