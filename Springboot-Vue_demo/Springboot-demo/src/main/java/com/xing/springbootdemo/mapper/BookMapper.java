package com.xing.springbootdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xing.springbootdemo.entity.Book;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: caidaxing
 * @Date: 2021/12/27/20:06
 * @Description:
 */
@Mapper
public interface BookMapper extends BaseMapper<Book> {
}
