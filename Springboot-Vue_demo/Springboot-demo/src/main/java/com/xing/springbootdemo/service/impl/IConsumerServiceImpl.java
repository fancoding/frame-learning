package com.xing.springbootdemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xing.springbootdemo.entity.Consumer;
import com.xing.springbootdemo.mapper.ConsumerMapper;
import com.xing.springbootdemo.service.IConsumerService;
import org.springframework.stereotype.Service;

/**
 * @Author: caidaxing
 * @Date: 2021/12/27/20:17
 * @Description:
 */
@Service
public class IConsumerServiceImpl extends ServiceImpl<ConsumerMapper, Consumer> implements IConsumerService {

}
