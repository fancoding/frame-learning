package com.xing.springbootdemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * @Author: caidaxing
 * @Date: 2021/12/27/18:23
 * @Description: 用户信息表
 */
@TableName("consumer")
@Data
public class Consumer {
    @TableId(type= IdType.AUTO)
    private Integer id;
    private String username;
    private String password;
    private String nickName;
    private int age;
    private String sex;
    private String address;
    private Integer role;
    /**
     *  TableField注解表示数据库不存在的字段，而Java中需要使用，加上这个注解就不会报错
     */
    @TableField(exist = false)
    private List<Book> bookList;

}
