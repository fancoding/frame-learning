package com.xing.springbootdemo.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xing.springbootdemo.common.Result;
import com.xing.springbootdemo.entity.Book;
import com.xing.springbootdemo.mapper.BookMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: caidaxing
 * @Date: 2021/12/27/20:00
 * @Description:
 */
@RestController
@RequestMapping("/book")
public class BookController {

    Logger logger = LoggerFactory.getLogger(BookController.class);

    @Resource
    BookMapper bookMapper;


    /**
     * 获取最新书籍信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<?> getById(@PathVariable Long id) {
        return Result.successWithData(bookMapper.selectById(id));
    }

    /**
     * 新增书籍
     * @param book
     * @return
     */
    @PostMapping
    public Result<?> save(@RequestBody Book book) {
        try {
            bookMapper.insert(book);
            return Result.successWithoutData();
        }catch (Exception e) {
            logger.info("新增失败");
            return Result.failed("新增失败");
        }
    }

    /**
     * 更新书籍信息
     * @param book
     * @return
     */
    @PutMapping
    public Result<?> update(@RequestBody Book book) {
        int i = bookMapper.updateById(book);
        if (i == 1) {
            return Result.successWithoutData();
        }
        logger.info("更新异常");
        return Result.failed("更新异常");
    }

    /**
     * 删除信息
     * @param id @PathVariable 注解来实现数据的获取
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Long id) {
        int i = bookMapper.deleteById(id);
        if (i == 1) {
            return Result.successWithoutData();
        }
        logger.info("删除异常");
        return Result.failed("删除异常");
    }

    /**
     * 查询 -> 按 名称 模糊查询
     * @param pageNum
     * @param pageSize
     * @param search
     * @return
     */
    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search) {
        // 模糊查询（昵称为 NULL ）也能匹配到数据
        LambdaQueryWrapper<Book> wrapper = Wrappers.<Book>lambdaQuery();
        if (StrUtil.isNotBlank(search)) {
            wrapper.like(Book::getName, search);
        }
        Page<Book> bookPage = bookMapper.selectPage(new Page<>(pageNum, pageSize), wrapper);
        return Result.successWithData(bookPage);
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @PostMapping("/deleteBatch")
    public Result<?> deleteBatch(@RequestBody List<Integer> ids) {
        try {
            bookMapper.deleteBatchIds(ids);
            return Result.successWithoutData();
        }catch (Exception e) {
            logger.info("批量删除异常");
            return Result.failed("批量删除异常");
        }
    }
}
