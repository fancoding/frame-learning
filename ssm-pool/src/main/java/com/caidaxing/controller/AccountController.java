package com.caidaxing.controller;

import com.caidaxing.domain.Account;
import com.caidaxing.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Author: cdx1999
 * @Date: 2021/09/26/15:49
 * @Description:
 */
@Controller
public class AccountController {

    /**
     * 按类型注入
     */
    @Autowired
    private AccountService accountService;

    @RequestMapping("/account/findAllAccount")
    public String findAllAccount(Model model) {
        System.out.println("Controller 表现层: 查询所有账户。。。");
        List<Account> list = accountService.findAllAccount();
        model.addAttribute("list", list);
        //在视图解析器中配置了前缀后缀
        return "list";
    }

    @RequestMapping("/account/save")
    public void save(Account account, HttpServletRequest request, HttpServletResponse response) throws IOException {
        accountService.saveAccount(account);
        response.sendRedirect(request.getContextPath()+"/account/findAllAccount");
        return;
    }

}
