package com.caidaxing.service.Impl;

import com.caidaxing.dao.IAccountdao;
import com.caidaxing.domain.Account;
import com.caidaxing.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: cdx1999
 * @Date: 2021/09/26/15:32
 * @Description:
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private IAccountdao iaccountdao;

    @Override
    public List<Account> findAllAccount() {
        System.out.println("Service 业务层：查询所有账户。。。");
        return iaccountdao.findAllAccount();
    }

    @Override
    public void saveAccount(Account account) {
        System.out.println("Service业务层：保存帐户...");
        //调用service中的saveAccount(account)方法
        iaccountdao.saveAccount(account);
    }
}
