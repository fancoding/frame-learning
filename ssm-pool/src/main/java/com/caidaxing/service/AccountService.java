package com.caidaxing.service;

import com.caidaxing.domain.Account;

import java.util.List;

/**
 * @Author: cdx1999
 * @Date: 2021/09/26/15:31
 * @Description:
 */
public interface AccountService {
    /**
     * 查询所有账户
     * @return
     */
    public List<Account> findAllAccount();

    /**
     * 保存账户信息
     * @param account
     */
    public void saveAccount(Account account);
}
