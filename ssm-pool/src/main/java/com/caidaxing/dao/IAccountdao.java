package com.caidaxing.dao;

import com.caidaxing.domain.Account;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: cdx1999
 * @Date: 2021/09/26/15:29
 * @Description:
 */
@Repository /*此注释代表这是一个持久层*/
public interface IAccountdao {

    @Select("select * from account")
    public List<Account> findAllAccount();

    @Insert("insert into account (name, money) value(#{name}, #{money})")
    public void saveAccount(Account account);
}
