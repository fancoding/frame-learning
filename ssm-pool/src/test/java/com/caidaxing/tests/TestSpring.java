package com.caidaxing.tests;

import com.caidaxing.service.AccountService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author: cdx1999
 * @Date: 2021/09/26/15:40
 * @Description:
 */
public class TestSpring {

    @Test
    public void run1() {
        ApplicationContext application = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        AccountService accountService = (AccountService) application.getBean("accountService");
        accountService.findAllAccount();
    }
}
